package by.epam.training;

import by.epam.training.analyzers.EqualAnalyzer;
import by.epam.training.exceptions.EqualAnalyzerException;
import by.epam.training.exceptions.ProxyFactoryGetInstanceException;
import by.epam.training.factories.ProxyFactory;
import by.epam.training.interfaces.IUser;
import by.epam.training.models.User;


/**
 * Package: by.epam.training
 * Project name: reflection-task
 * Created by Mikita Isakau
 * Creation date: 24.11.2016
 */
public final class Main {
    private Main() { }

    /**
     * Main runnable class method.
     * @param args the args of command-line
     */
    public static void main(final String[] args) {
        System.out.println("PART 1:");
        try {
            IUser proxyUser = (IUser) ProxyFactory.getInstanceOf(IUser.class);
            proxyUser.getEmail();
            proxyUser.setLogin("Some login");
        } catch (ProxyFactoryGetInstanceException e) {
            e.printStackTrace();
        }

        System.out.println("PART 2:");
        User firstUser = new User(),
                secondUser = new User();
        String user = "harley26";

        firstUser.setId(10);
        firstUser.setLogin(user);
        firstUser.seteMail("harley26.gold@gmail.com");
        firstUser.setFullName("James Kirk");

        secondUser.setId(10);
        secondUser.setLogin(user);
        secondUser.seteMail("harley26.gold@gmail.com");
        secondUser.setFullName("John Dalvik");

        System.out.println("First user: " + firstUser);
        System.out.println("Second user: " + secondUser);

        try {
            System.out.println("The first and second class is same for some fields: "
                    + EqualAnalyzer.equalObjects(firstUser, secondUser));
        } catch (EqualAnalyzerException e) {
            e.printStackTrace();
        }
    }
}
