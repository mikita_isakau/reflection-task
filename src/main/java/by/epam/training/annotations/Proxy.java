package by.epam.training.annotations;

import java.lang.annotation.*;

/**
 * Package: by.epam.training.annotations
 * Project name: reflection-task
 * Created by Mikita Isakau
 * Creation date: 24.11.2016
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface Proxy {
    /**
     * Value of class name, implemented InvocationHandler.
     * * @return the string class name
     */
    String value() default "";
}
