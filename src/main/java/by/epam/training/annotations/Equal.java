package by.epam.training.annotations;

import by.epam.training.util.CompareBy;

import java.lang.annotation.*;

/**
 * Package: by.epam.training.annotations
 * Project name: reflection-task
 * Created by Mikita Isakau
 * Creation date: 24.11.2016
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Inherited
public @interface Equal {
    /**
     * Compare  by reference or value.
     * @return the compare strategy
     */
    CompareBy compare() default CompareBy.VALUE;
}
