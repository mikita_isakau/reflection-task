package by.epam.training.interfaces;

import by.epam.training.annotations.Proxy;

/**
 * Package: by.epam.training.interfaces
 * Project name: reflection-task
 * Created by Mikita Isakau
 * Creation date: 24.11.2016
 */
@Proxy("by.epam.training.handlers.UserInvocationHandler")
public interface IUser {
    /**
     * Sets login.
     * @param newLogin the new login
     */
    void setLogin(String newLogin);

    /**
     * Gets login.
     * @return the login
     */
    String getLogin();

    /**
     * Sets email.
     * @param newEmail the new email
     */
    void setEmail(String newEmail);

    /**
     * Gets email.
     *
     * @return the email
     */
    String getEmail();

    /**
     * Sets phone.
     *
     * @param newLogin the new login
     */
    void setPhone(Integer newLogin);

    /**
     * Gets phone.
     * @return the phone
     */
    Integer getPhone();
}
