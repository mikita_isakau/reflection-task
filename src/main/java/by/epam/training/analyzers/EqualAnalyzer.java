package by.epam.training.analyzers;

import by.epam.training.exceptions.EqualAnalyzerException;
import by.epam.training.util.CompareBy;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Objects;

/**
 * Package: by.epam.training.analyzers
 * Project name: reflection-task
 * Created by Mikita Isakau
 * Creation date: 25.11.2016
 */
public final class EqualAnalyzer {
    private static final Class EQUAL_ANNOTATION_CLASS = by.epam.training.annotations.Equal.class;

    private static Class<?> targetClass = null;

    private EqualAnalyzer() { }

    /**
     * Equal objects boolean.
     * @param first  the first
     * @param second the second
     * @return the boolean
     * @throws EqualAnalyzerException when was something wrong
     */
    public static boolean equalObjects(final Object first, final Object second)
            throws EqualAnalyzerException {
        if (!Objects.deepEquals(first.getClass(), second.getClass())) {
            return false;
        }
        targetClass = first.getClass();
        Field[] fieldsOfTargetClass = targetClass.getDeclaredFields();
        boolean isEqualClasses = true;
        for (Field field : fieldsOfTargetClass) {
            field.setAccessible(true);
            if (!field.isAnnotationPresent(EQUAL_ANNOTATION_CLASS)) {
                continue;
            }
            Object firstValue = null,
                secondValue = null;
            try {
                firstValue = field.get(first);
                secondValue = field.get(second);
            } catch (IllegalAccessException e) {
                throw new EqualAnalyzerException("Accessing to field was failed", e);
            }
            Annotation compareValueAnnotation = field.getAnnotation(EQUAL_ANNOTATION_CLASS);
            Method compareAnnotationDefaultMethod = null;
            try {
                compareAnnotationDefaultMethod = EQUAL_ANNOTATION_CLASS.getMethod("compare", new Class[0]);
            } catch (NoSuchMethodException e) {
                throw new EqualAnalyzerException("Error with invoking the method from "
                        + "@Equal annotation class", e);
            }
            CompareBy valueOfAnnotation = null;
            try {
                valueOfAnnotation = (CompareBy) compareAnnotationDefaultMethod.invoke(compareValueAnnotation);
            } catch (IllegalAccessException | InvocationTargetException e) {
                throw new EqualAnalyzerException("Error when invoke annotation value", e);
            }
            isEqualClasses = valueOfAnnotation.compare(firstValue, secondValue);
            if (!isEqualClasses) {
                return isEqualClasses;
            }
        }
        return isEqualClasses;
    }
}
