package by.epam.training.util;


/**
 * Package: by.epam.training.models
 * Project name: reflection-task
 * Created by Mikita Isakau
 * Creation date: 25.11.2016
 */
public enum CompareBy {
    /**
     * Reference compare by enum.
     */
    REFERENCE {
        @Override
        public boolean compare(final Object obj1, final Object obj2) {
            return obj1 == obj2;
        }
    },

    /**
     * Value compare by enum.
     */
    VALUE {
        @Override
        public boolean compare(final Object obj1, final Object obj2) {
            return obj1.equals(obj2);
        }
    };

    /**
     * Compare objects for equal.
     *
     * @param obj1 the first object
     * @param obj2 the second object
     * @return the boolean - equals objects or not
     */
    public abstract boolean compare(Object obj1, Object obj2);
}
