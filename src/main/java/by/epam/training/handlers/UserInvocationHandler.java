package by.epam.training.handlers;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Objects;

/**
 * Package: by.epam.training.handlers
 * Project name: reflection-task
 * Created by Mikita Isakau
 * Creation date: 24.11.2016
 */
public class UserInvocationHandler implements InvocationHandler {

    @Override
    public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {
        System.out.println("The " + method.getName() + " has invoked in the UserInvocationHandler");

        if (Objects.nonNull(args)) {
            System.out.println("Arguments:");
            for (Object arg : args) {
                System.out.println("Argument: " + arg);
            }
        }
        return "";
    }
}
