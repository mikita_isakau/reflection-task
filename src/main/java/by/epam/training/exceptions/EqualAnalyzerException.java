package by.epam.training.exceptions;

/**
 * Package: by.epam.training.exceptions
 * Project name: reflection-task
 * Created by Mikita Isakau
 * Creation date: 25.11.2016
 */
public class EqualAnalyzerException extends Exception {
    /**
     * Instantiates a new Equal analyzer exception.
     */
    public EqualAnalyzerException() {
    }

    /**
     * Instantiates a new Equal analyzer exception.
     *
     * @param message the message of exception
     */
    public EqualAnalyzerException(final String message) {
        super(message);
    }

    /**
     * Instantiates a new Equal analyzer exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public EqualAnalyzerException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
