package by.epam.training.exceptions;

/**
 * Package: by.epam.training.exceptions
 * Project name: reflection-task
 * Created by Mikita Isakau
 * Creation date: 25.11.2016
 */
public class ProxyFactoryGetInstanceException extends Exception {
    /**
     * The constant MAIN_MESSAGE.
     */
    public static final String MAIN_MESSAGE = "Exception with getting instance from ProxyFactory: ";

    /**
     * Instantiates a new Proxy factory get instance exception.
     *
     * @param message the message
     */
    public ProxyFactoryGetInstanceException(final String message) {
        super(MAIN_MESSAGE + message);
    }

    /**
     * Instantiates a new Proxy factory get instance exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public ProxyFactoryGetInstanceException(final String message, final Throwable cause) {
        super(MAIN_MESSAGE + message, cause);
    }
}
