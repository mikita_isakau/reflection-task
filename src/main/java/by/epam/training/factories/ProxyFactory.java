package by.epam.training.factories;

import by.epam.training.exceptions.ProxyFactoryGetInstanceException;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Package: by.epam.training.factories
 * Project name: reflection-task
 * Created by Mikita Isakau
 * Creation date: 24.11.2016
 */
public final class ProxyFactory {
    private static final Class PROXY_ANNOTATION_CLASS = by.epam.training.annotations.Proxy.class;

    private ProxyFactory() { }
    /**
     * Gets instance of Proxy class, where class name declared in @Proxy annotation.
     *
     * @param invokeClass the interface
     * @return the instance of @param invokeClass
     * @throws ProxyFactoryGetInstanceException the proxy factory get instance exception
     */
    public static Object getInstanceOf(final Class invokeClass)
            throws ProxyFactoryGetInstanceException {

        if (!invokeClass.isInterface()) {
            throw new ProxyFactoryGetInstanceException("The current class in not a interface!");
        }
        if (!invokeClass.isAnnotationPresent(PROXY_ANNOTATION_CLASS)) {
            throw new ProxyFactoryGetInstanceException("This class doesn't have @Proxy annotation");
        }

        Annotation proxyAnnotation = invokeClass.getAnnotation(PROXY_ANNOTATION_CLASS);
        Class<? extends Annotation> proxyAnnotationClass = proxyAnnotation.annotationType();

        final Method methodOfProxyAnnotationClass;
        try {
            methodOfProxyAnnotationClass = proxyAnnotationClass.getMethod("value", new Class[0]);
        } catch (NoSuchMethodException e) {
            throw new ProxyFactoryGetInstanceException("Error with invoking the method from proxy annotation class", e);
        }

        final String invocationHandlerClassName;
        try {
            invocationHandlerClassName = (String) methodOfProxyAnnotationClass.invoke(proxyAnnotation);
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new ProxyFactoryGetInstanceException("Error with getting invocationHandler's class name", e);
        }

        Class<?> invocationHandlerClass = null;
        try {
            invocationHandlerClass = Class.forName(invocationHandlerClassName);
        } catch (ClassNotFoundException e) {
            throw new ProxyFactoryGetInstanceException("The invocation handler class is not found!", e);
        }

        InvocationHandler invocationHandlerInstance = null;
        try {
            invocationHandlerInstance = (InvocationHandler) invocationHandlerClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new ProxyFactoryGetInstanceException("Error with instantiation invocation handler class", e);
        }

        return Proxy.newProxyInstance(ClassLoader.getSystemClassLoader(),
                new Class[] {invokeClass},
                invocationHandlerInstance);
    }
}
