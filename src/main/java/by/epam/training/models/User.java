package by.epam.training.models;

import by.epam.training.annotations.Equal;
import by.epam.training.util.CompareBy;

/**
 * Package: by.epam.training.models
 * Project name: reflection-task
 * Created by Mikita Isakau
 * Creation date: 25.11.2016
 */
public class User {

    @Equal(compare = CompareBy.VALUE)
    private int id;

    @Equal(compare = CompareBy.REFERENCE)
    private String login;

    private String fullName;

    @Equal(compare = CompareBy.VALUE)
    private String eMail;

    /**
     * Gets id of user.
     * @return the user's id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     * @return the id
     */
    public User setId(final int id) {
        this.id = id;
        return this;
    }

    /**
     * Gets login.
     *
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * Sets login.
     *
     * @param login the login
     * @return the login
     */
    public User setLogin(final String login) {
        this.login = login;
        return this;
    }

    /**
     * Gets full name.
     *
     * @return the full name
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * Sets full name.
     *
     * @param fullName the full name
     * @return the full name
     */
    public User setFullName(final String fullName) {
        this.fullName = fullName;
        return this;
    }

    /**
     * Gets mail.
     *
     * @return the mail
     */
    public String geteMail() {
        return eMail;
    }

    /**
     * Sets mail.
     *
     * @param eMail the e mail
     * @return the mail
     */
    public User seteMail(final String eMail) {
        this.eMail = eMail;
        return this;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("User{");
        sb.append("id=").append(id);
        sb.append(", login='").append(login).append('\'');
        sb.append(", fullName='").append(fullName).append('\'');
        sb.append(", eMail='").append(eMail).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
